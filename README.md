# Avaliação prática

## Tarefa

Faça uma API que possua 2 endpoints:

1. GET http://localhost:8081/dolar
    - esse endpoint retorna o valor atual do dolar via requisições HTTP/HTTPS;
    - deve ser mostrado na tela do Browser através de uma requisição GET.

2. O segundo endpoint será um listener para serviço de email:
    - a API deverá receber uma mensagem em JSON e cadastrar o remetente desta mensagem;
    - enviar emails periódicos para o remetente cadastrado informando o preço do dolar;
    - passar as informações de periodicidade do email seguindo o template abaixo:

*Exemplo: A aplicação enviará 1 email para o remetente a cada 10 segundos durante 5 minutos:*

```javascript
{
    "remetente": "email@host.com",
    "frequencia": 10,   // em segundos
    "periodo": 5        // em minutos
}
```

## Requisitos

- Configurar o packaje.json para rodar a aplicação com comando:
```shell
$ npm start
```
- Utililização de NodeJS/ES6;
- Requisições em formato JSON;
- Disponibilizar o codigo em um repositorio publico do GitHub.

## Critérios levados em conta na avaliação:

- Conclusão da tarefa;
- Soluções inteligentes e inovadoras;
- Utilização de patterns e organização de código;
- Segurança de código;
- Utilização correta dos 'status code' no padrão RESTfull;
- Escolha das bibliotecas utilizadas;
- Maturidade de código.

# Boa sorte!#

